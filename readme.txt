REQUIREMENTS
The application must run on a Linux distribution or Ubuntu

INSTALLATION
1.Must install all dependencies specified as in the packages.json file (located in server/node_modules). Installed using "npm install"
2. Install node.js from https://nodejs.org/

RUNNING THE APPLICATION
1. Run "java -cp '.:sqlite.jdbc.3.8.7.jar' ParseData" in server/db to get application listening to stream
2. Run "node app.js" in server/db to ...

RUNNING THE APPLICATION
1. Go to (Your server's DNS)/AndreeaApp
2. Now possible to access web application through this link

========================================================================================================================
														LICENCES														
========================================================================================================================
node.js - http://opensource.org/licenses/MIT

Bootstrap - http://opensource.org/licenses/MIT

d3.js - http://opensource.org/licenses/BSD-2-Clause




