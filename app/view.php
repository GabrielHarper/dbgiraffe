<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ForexSpot</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>

  </head>

  <body>
  
  
    <nav class="navbar navbar-fixed-top navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html">ForexSpot</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.html">Home</a></li>
            <li><a href="application.html">Application</a></li>
            <li class="active"><a href="#about" target="_top">View</a></li>
          </ul>
        </div><!-- /.nav-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->

    <div class="container">

      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
          
          <div class="jumbotron spacing">
	            <h3>Time/price view</h3>
	            <div>
	            	<ul>
	            			<li>View based on traders communication, based on a highly frequented bases.</li>
	            			<li>Highlight suspicious trades on a time/price graph</li>
	            			<li>Notification system</li>
	            			<li>Further views</li>
	            	</ul>
	            </div>
	       </div>
          
         <div class="row">
            <div class="col-xs-12 col-lg-12">
            	<div class="box">
	            
	              <h2>Graph</h2>
	              <p>Notification system built as a click view system with D3.js</p>
	              <p>Include in description why that trade/comm is suspicious by pointing out one of the macilicious trades spotting methods</p>
	              
	            </div>
            </div>
          </div><!--/row-->
          
          <div class="row">
          	 <div class="col-xs-12 col-lg-12">
            	 <div class="box">
		          
		              <h2>Table of data collected from the Database & updated every x minutes</h2>
		              <p>Styled table containing data from the CSV file</p>
		              <p></p>
		              
		          </div>
		       </div>
          </div>
          
          
        </div>

        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
          <div class="list-group">
            <p class="list-group-item active">Choose time interval</p>   
            <a href="#" class="list-group-item">5 minutes</a> 
            <a href="#" class="list-group-item">10 minutes</a>
            <a href="#" class="list-group-item">30 minutes</a>
            <a href="#" class="list-group-item">60 minutes</a>
            <a href="#" class="list-group-item">120 minutes</a>
          </div>
        </div><!--/.sidebar-->
        
        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
	          <div class="list-group">
	            <p class="list-group-item active">Upload CSV</p>
	            <div class="list-group-item" >
		            <form action="upload.php" method="post" enctype="multipart/form-data">
						  	 <input type="file" name="fileToUpload" id="fileToUpload">
						    <input type="submit" value="Upload file" name="submit">
						</form>
					</div>
	          </div>
        </div><!--/.sidebar-->
      </div><!--/row-->

     
    		 </div>
      
      
      <hr>
      
     

	  <div class="row">
        <div class="col-xs-12 col-lg-12">
		      <footer>
		        <p>&copy; DBSwan presented in Week 10, Term 2, 2015</p>
		      </footer>
        </div>
     </div>

    </div><!--/.container-->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="css/bootstrap/js/bootstrap.min.js"></script>

    <script src="index.js"></script>
  </body>
</html>
