

// In the future loading will be done from a method call something like: var trades = db.getTrades()
// TODO: re-do the spread dimension (atm you assume that the objects will have a spread attribute)
var d3 = require('d3');
var crossfilter = require('crossfilter');
var dc = require('dc');
// Creates a live updating table which contains the raw data, and updates automatically as users select filters.
// TODO: Currently not all the entries display, fix this bug. 
// I would recommend inspecting with chrome tools to see if the row tags are being filled.



var updateInterval = 60; // in seconds

function createDataTable(dateDim, trades)
{
	var datatable = dc.dataTable("#dc-data-table");
	datatable
	.width(700)
	.height(800)
	.dimension(dateDim)
	.group(function(d) {return "The following trades were all executed at minute " + d.Year;})
	.size(1000) // The data table will currently only display 1000 trades, ignoring the rest. 
	// dynamic columns creation using array of closures
	.columns([
		function(d) { return d.Time.getDate() + "/" + (d.Time.getMonth()+1) + "/" + d.Time.getFullYear() + " " + d.Time.getHours() + ":" + d.Time.getMinutes() + ":" + d.Time.getSeconds(); },
		function(d) { return d.BuyerEmail; },
		function(d) { return d.SellerEmail; },
		function(d) { return d.Price; },
		function(d) { return d.Currency; },
		function(d) { return d.TimeradeSize; },
		function(d) { return d.Symbol; },
		function(d) { return d.Sector; },
		function(d) { return d.Bid; },
		function(d) { return d.Ask; },
		function(d) { return d.Spread; }
		]);
}

// Creates a chart with volume of stock traded as y axis and time as x axis.
function createPriceChart(dateDim, trades)
{
	var minDate = dateDim.bottom(1)[0].Time;
	var maxDate = dateDim.top(1)[0].Time;
	var volume = dateDim.group(
		function(date) {
		 	date.setSeconds(date.getSeconds() + Math.round(date.getMilliseconds()/60));
		 	date.setMilliseconds(0);
		 	return date;
		})
	.reduceSum(dc.pluck('TradeSize'));
	var priceChart = dc.lineChart("#chart-line-priceday");
	priceChart
	.width(800).height(230)
	.dimension(dateDim)
	.group(volume)
	.elasticX(true)
	.elasticY(true)
	.x(d3.time.scale().domain([minDate,maxDate]))
	.brushOn(false)
	.yAxisLabel("Vol of stocks");
}

// Creates a chart with the spreads of trades.
function createSpreadChart(dateDim, trades)
{
	var spreadDim = trades.dimension(function(d) { return d.Spread;});
	var minSpread = spreadDim.bottom(1)[0].Spread;
	var maxSpread = spreadDim.top(1)[0].Spread;
	var spreadGroupCount = spreadDim.group().reduceCount(function(d) { return d.Spread;});
	var spreadChart = dc.barChart("#chart-bar-spread");
	spreadChart
	.width(800).height(230)
	.dimension(spreadDim)
	.group(spreadGroupCount)
	.elasticX(true)
	.elasticY(true)
	.x(d3.scale.linear().domain([minSpread,maxSpread]))
	.brushOn(true)
	.xAxisLabel("Spread of trade")
	.yAxisLabel("Number of trades with spread");
}

// TODO: change name
function createYearRingChart(trades)
{
	var yearDim = trades.dimension(function(d) { return +d.Year;});
	var year_total = yearDim.group().reduceSum(function(d) { return d.TradeSize;});
	var yearRingChart = dc.pieChart("#chart-ring-year");
	yearRingChart
		.width(150).height(150)
		.dimension(yearDim)
		.group(year_total)
		.innerRadius(30);
}

// Creates a pie chart, where users can select data by sector.
function createSectorChart(trades) 
{
	var sectorDim = trades.dimension(function(d) { return d.Sector;});
	var sector_total = sectorDim.group().reduceSum(function(d) { return d.TradeSize;});
	var sectorChart = dc.pieChart("#chart-sector");
	sectorChart
		.width(150).height(150)
		.dimension(sectorDim)
		.group(sector_total)
		.innerRadius(30)
}


function generateVisualisations(data)
{
	// TODO: Have this done on the database end to speed up responsiveness.
	var secondsBefore = new Date().getTime() / 1000;
	console.log(secondsBefore);
	for (var p=0; p < data.length; p++)
	{
		var correctDateFormat = Date.parse((data[p].Time).replace(/\s+/g, 'T'));
		data[p].Time = new Date(correctDateFormat);
		data[p].Price = Math.round(data[p].Price);
		data[p].Ask = Math.round(data[p].Ask);
		data[p].Bid = Math.round(data[p].Bid);
		data[p].Spread = Math.abs(data[p].Ask - data[p].Bid);
		data[p].Year = data[p].Time.getMinutes();
	}
	var secondsAfter = new Date().getTime() / 1000;
	console.log(secondsAfter - secondsBefore);
	var trades = crossfilter(data);
	// These time/date dimensions are used in all functions, so are outside function scope.
	var dateDim = trades.dimension(function(d) { return d.Time;});


	createPriceChart(dateDim,trades);
	createSpreadChart(dateDim,trades);
	createYearRingChart(trades);
	createSectorChart(trades);
	createDataTable(dateDim,trades);
	dc.renderAll();
	window.setInterval(function() {
		console.log("I should be printing every" + updateInterval +" seconds");
		console.log("value of trades is " + JSON.stringify(trades));
		updateView(trades);
	},updateInterval*1000)
}
function updateView(trades)
{
	// add trades form the last second to the crossfilter.
	$.ajax({
		type: "POST",
		url: serverName + "/" + "queryTrades",
		data: {time: updateInterval},
		success: function (data) {
			for (var p=0; p < data.length; p++)
			{
				var correctDateFormat = Date.parse((data[p].Timeime).replace(/\s+/g, 'T'));
				data[p].Time = new Date(correctDateFormat);
				data[p].Price = Math.round(data[p].Price);
				data[p].Ask = Math.round(data[p].Ask);
				data[p].Bid = Math.round(data[p].Bid);
				data[p].Price = Math.round(data[p].Price);
				data[p].Spread = Math.abs(data[p].Ask - data[p].Bid);
				data[p].Year = data[p].Time.getMinutes();
			}
			trades.add(data);
			dc.redrawAll();
		}
	});
}

function resetFilters()
{
	console.log("resetting filters");
	dc.filterAll();
}

function resetCharts()
{
	$('#chart-bar-spread').empty();
	$('#chart-line-priceday').empty();
	$('#chart-ring-year').empty();
	$('#chart-sector').empty();
	$('#feedTable').empty();

}
function loadData(timeInterval) // time interval is given in minutes
{
	$.ajax({
		type: "POST",
		url: serverName + "/" + "queryTrades",
		data: {time: timeInterval*60},
		success: function (data) {
			generateVisualisations(data);
		}
	});
}

loadData(5); // load data for 5 minutes to begin with 

$("#resetButton").click(resetFilters);

$("#selectTimeInterval").click(function(event)
{
	var targetID = event.target.id;
	var elem = $("#" + targetID);
	var minutes = elem.val();
	console.log("num of Minutes selected is " + minutes);
	resetFilters();
	resetCharts();
	loadData(minutes);
});

