$('#uploadTradeData').submit( function (event) {
    event.preventDefault();

    var fileSelect = document.getElementById('trades-select');
    var file = fileSelect.files[0];

    var formData = new FormData();
    formData.append('file', file);

    $.ajax({
	url: serverName + "/upload",
	type: 'post',
        data: formData,
	processData: false,
	contentType: false,
        success: function(data) {
            console.log(data);
        }
    });
    return false;
});

$('#uploadCommunicationData').submit( function (event) {
    event.preventDefault();

    var fileSelect = document.getElementById('comms-select');
    var file = fileSelect.files[0];

    var formData = new FormData();
    formData.append('file', file);

    $.ajax({
	url: serverName + "/upload",
	type: 'post',
        data: formData,
	processData: false,
	contentType: false,
        success: function(data) {
            console.log(data);
        }
    });
    return false;
});
