more "$1" | sed 's/\.[0-9]\{6\}//g' > tmpfile
cat tmpfile > $1
rm tmpfile

echo " 
.mode csv
.import $1 TempTrade
" > tempsql.sql

sqlite3 trades.db < uploadtradeschema.sql
sqlite3 trades.db < tempsql.sql
rm tempsql.sql
