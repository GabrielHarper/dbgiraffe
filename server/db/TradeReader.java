/**
 * Created by Isheanesu Gambe on 17/02/2015.
 *
 * Class will connect to the trades and communication sockets, and will read from the socket, processing the feeds line by line.
 * Each line is put in an array which is then passed on the the QueryParser class
 */
import java.io.*;
import java.net.*;
import java.sql.SQLException;

public class TradeReader extends Thread{
    //Method will read from the connected socket and create an array for each new line encountered int the stream. This is then passed on to the query parser
    public void run(){
        try {
            SocketConnection conn = new SocketConnection(80);
            BufferedReader dataStream = conn.connect();
            String response;
            QueryParser qp = new QueryParser();
            //Variable parse is used to avoid the first line of the stream (field headers) being passed on to the query parser
            boolean parse = false;

            while ((response = dataStream.readLine()) != null) {
                if (parse) {
                    //split() will separate the fields in a line of the feed by commas, allowing each index in the array to represent a different field
                    String[] tradeInfo = response.split(",");
                    qp.parseTradeQuery(tradeInfo);
                } else {
                    parse = true;
                }
            }
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }
}