DROP TABLE Trade;
CREATE TABLE Trade (
	TradeID 	INTEGER		NOT NULL ,
	Time		DATETIME,			--of the form yyyy-MM-dd HH:mm:ss
	BuyerEmail	VARCHAR(30)	NOT NULL,
	SellerEmail	VARCHAR(30)	NOT NULL,
	Price		FLOAT,
	TradeSize	INTEGER, 			--'Size' is apparently a reserved word in SQL!
	Currency	VARCHAR(4),
	Symbol		VARCHAR(10),
	Sector		VARCHAR(30),
	Bid		FLOAT,
	Ask		FLOAT,
  
	PRIMARY KEY(TradeID)
);
DROP TABLE Communication;
CREATE TABLE Communication (
	CommunicationID	INTEGER		NOT NULL ,
	Time		DATETIME,	
	Sender		VARCHAR(255)	NOT NULL,
	Receiver	VARCHAR(255),
	
	PRIMARY KEY(CommunicationID)
);

DROP TABLE IF EXISTS UploadTrade;
CREATE TABLE UploadTrade (
	Time		DATETIME,			--of the form yyyy-MM-dd HH:mm:ss
	BuyerEmail	VARCHAR(30),
	SellerEmail	VARCHAR(30),
	Price		FLOAT,
	TradeSize	INTEGER, 			--'Size' is apparently a reserved word in SQL!
	Currency	VARCHAR(4),
	Symbol		VARCHAR(10),
	Sector		VARCHAR(30),
	Bid		FLOAT,
	Ask		FLOAT
);


DROP TABLE UploadCommunication;
CREATE TABLE UploadCommunication (
	Time		DATETIME,	
	Sender		VARCHAR(255),
	Receiver	TEXT
);
