DROP TABLE IF EXISTS UploadCommunication;
CREATE TABLE UploadCommunication (
	Time		DATETIME,	
	Sender		VARCHAR(255),
	Receiver	TEXT
);
