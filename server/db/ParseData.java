import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Isheanesu Gambe on 17/02/2015.
 *
 * Class to run the application
 */
public class ParseData{
    public static void main(String[] args) {
        TradeReader tr = new TradeReader();
        CommReader cr = new CommReader();

        tr.start();
        cr.start();
    }
}
