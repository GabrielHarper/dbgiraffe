DROP TABLE IF EXISTS UploadTrade;
CREATE TABLE UploadTrade (
	Time		DATETIME,			--of the form yyyy-MM-dd HH:mm:ss
	BuyerEmail	VARCHAR(30),
	SellerEmail	VARCHAR(30),
	Price		FLOAT,
	TradeSize	INTEGER, 			--'Size' is apparently a reserved word in SQL!
	Currency	VARCHAR(4),
	Symbol		VARCHAR(10),
	Sector		VARCHAR(30),
	Bid		FLOAT,
	Ask		FLOAT
);
