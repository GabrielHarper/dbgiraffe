import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Isheanesu Gambe on 19/02/2015.
 *
 * QueryParser parser class will parse data on trades and communications. Has two methods, parseTradeQuery() and parseCommQuery()
 * which take arrays, and prepare an SQL statement which then binds the appropriate parsed value of the array
 */

public class QueryParser {

    private SQLite db;

    //Constructor creates a MySQL object which signs you into the localhost server database
    public QueryParser()
        throws SQLException{
        db = new SQLite();

    }

    //Method to parse trade queries. Works by using a prepared statement which allows you to parse values before binding them to the query
    public void parseTradeQuery(String[] array){
        try {
            PreparedStatement ps = db.getConnection().prepareStatement("INSERT INTO Trade VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            //Method from ps allows us to bind the parameter in a query to a certain type (setX). Before setting, can parse value from string
            //to the appropriate value;

            ps.setString(1, array[0]); //Time
            ps.setString(2, array[1]); //BuyerID
            ps.setString(3, array[2]); //SellerID
            ps.setFloat(4, Float.parseFloat(array[3])); //Price
            ps.setInt(5, Integer.parseInt(array[4]));//Trade size
            ps.setString(6, array[5]);//Currency varchar
            ps.setString(7, array[6]);//Symbol varchar
            ps.setString(8,array[7]); //Sector
            ps.setFloat(9, Float.parseFloat(array[8]));//Bid float
            ps.setFloat(10, Float.parseFloat(array[9]));//Ask float

            //Execute the query
            ps.executeUpdate();
        }
        //Catch any exception thrown when attempting to execute the query
        catch(SQLException e){
            System.out.println("Message: " + e.getMessage());
        }
    }

    //Method to parse communication queries. Works by using a prepared statement which allows you to parse values before binding them to the query
    public void parseCommQuery(String[] array){
        try {
            //Store the recipients of the email in another array, split by a period
            String[] receivers = array[2].split(";");

            //For each recipient, insert a new row into the table. When a sender sends to multiple people, a new row is created for each
            //recipient. However, sender email and fields for date and microseconds will be the same for all recipients
            for(int i = 0; i < receivers.length; i++) {
                PreparedStatement ps = db.getConnection().prepareStatement("INSERT INTO Communication VALUES(NULL, ?, ?, ?)");
                ps.setString(1, array[0]); //Time
                ps.setString(2, array[1]); //Sender
                ps.setString(3, receivers[i]); //Receiver
                ps.executeUpdate();
            }
        }
        catch(SQLException e){
            System.out.println("Message: " + e.getMessage());
        }
    }
}
