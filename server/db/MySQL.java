/**
 * Created by Isheanesu Gambe on 18/02/2015.
 *
 * Class provides way of specifiying which database is to be connected to, and can create connections. The class will also return created connections
 */

import java.sql.*;

public class MySQL {

    private final String user;
    private final String password;
    private final String host;
    private Connection conn;

    //Constructor will specify which database should be connected to
    public MySQL(String host, String user, String password)
        throws SQLException{
        this.host = host;
        this.user = user;
        this.password = password;
        conn = createConnection();
    }

    //Method to create  a connection to the specified database
    public Connection createConnection()
        throws SQLException{
        Connection conn = null;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://" + this.host, this.user, this.password);
        }catch(ClassNotFoundException e){
            System.out.println("Could not load the driver");
        }
        return conn;
    }

    public Connection getConnection() {
        return conn;
    }
}
