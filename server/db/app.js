//requires the node library sqlite3 'npm install sqlite3'
//and body-parser 'npm install --save body-parser'
//and multer

//run with node app.js

//curl --data "time=N" http://supersonic-comet-33-194586.euw1-2.nitrousbox.com:3000
//Where N is the time in seconds we want to go back

//can specify time (as above), or mintime/maxtime 

var express = require('express');
var app = express();
var router = express.Router();

var bodyParser = require('body-parser');
var multer = require ('multer');

var fs = require('fs');

var done = false;


app.use(bodyParser.json({
    limit: 500000000
}));
app.use(bodyParser.urlencoded({
    limit: 500000000,
    extended: true
}));


var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('trades.db');

db.serialize(function() {
    db.run("PRAGMA journal_mode=WAL");
});

function queryComms(table, req, res) {
    var conditions = []

    var sqlCall = "SELECT * FROM " + table;

    if (req.body.time) {
		var d = new Date();
		var s = d.getTime();

		//we want all trades after this time
		s -= 1000 * req.body.time;
		d.setTime(s);

		var year = d.getFullYear();
		var month = pad(d.getMonth() + 1);
		var day = pad(d.getDate());
		var hour = pad(d.getHours());
		var minute = pad(d.getMinutes());
		var second = pad(d.getSeconds());
		var sqlTime = year+ "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;

		conditions.push(" Time > \"" + sqlTime + "\" ");
    }
    else { 
	if (req.body.mintime) {
	    conditions.push(" Time > \"" + req.body.mintime + "\" ");
	}
	if (req.body.maxtime) {
	    conditions.push(" Time < \"" + req.body.maxtime + "\" ");
	}
    }
    
    if (req.body.trader) {
	conditions.push("(Sender = \"" + req.body.trader + "\" " + " OR " + "Receiver = \"" + req.body.trader + "\") ");
    }

    if (conditions.length >= 1) {
	sqlCall += " WHERE " + conditions[0];
    }

    if (conditions.length >= 2) {
	for (i = 1; i < conditions.length; i ++) {
	    sqlCall += " AND " + conditions[i];
	}
    }

    sqlCall += ";";
    console.log("Calling: " + sqlCall);

    db.all(sqlCall, function(err, rows) {
    	if(err)
    	{
    		console.log("error has occurred, see below");
    		console.log(err);
    	}
	res.send(rows);
    });

}

app.post("/queryComms", function (req, res) {
    queryComms("communication", req, res);
});

app.post("/queryUploadComms", function (req, res) {
    queryComms("uploadcommunication", req, res);
});

function queryTrades(table, req, res) {
    var conditions = []

    var sqlCall = "SELECT * FROM " + table
   
    if (req.body.time) {
		var d = new Date();
		var s = d.getTime();

		//we want all trades after this time
		s -= 1000 * req.body.time;
		d.setTime(s);

		var year = d.getFullYear();
		var month = pad(d.getMonth() + 1);
		var day = pad(d.getDate());
		var hour = pad(d.getHours());
		var minute = pad(d.getMinutes());
		var second = pad(d.getSeconds());
		var sqlTime = year+ "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;

		conditions.push(" Time > \"" + sqlTime + "\" ");
    }

    else { 
	if (req.body.mintime) {
	    conditions.push(" Time > \"" + req.body.mintime + "\" ");
	}
	if (req.body.maxtime) {
	    conditions.push(" Time < \"" + req.body.maxtime + "\" ");
	}
    }
    
    if (req.body.buyer) {
		conditions.push(" BuyerEmail = \"" + req.body.buyer +"\" ");
    }

    if (req.body.seller) {
		conditions.push(" SellerEmail = \"" +  req.body.buyer + "\" ");
    }

    if (req.body.minPrice) {
		conditions.push(" Price > \"" +  req.body.minPrice + "\" ");
    }	
    
    if (req.body.maxPrice) {
		conditions.push(" Price < \"" +  req.body.maxPrice + "\" ");
    }	
    
    if (conditions.length >= 1) {
		sqlCall += " WHERE " + conditions[0];
    }

    if (conditions.length >= 2) {
		for (i = 1; i < conditions.length; i ++) {
	    	sqlCall += " AND " + conditions[i];
		}
    }

    sqlCall += ";";
    console.log("Calling: " + sqlCall);

    db.all(sqlCall, function(err, rows) {
    	if(err)
    	{
    		console.log("error has occurred, see below");
    		console.log(err);
    	}
	res.send(rows);
    });
}

app.post("/queryTrades", function (req, res) {
    queryTrades("trade", req, res);
});

app.post("/queryUploadTrades", function (req, res) {
    queryTrades("uploadtrade", req, res);
});

var done = false;

function runschema(filename, callback) {
    fs.readFile(filename, 'utf8', function (err, data) {
	if (err) {
	    console.log(err);
	}
	db.exec(data, callback);
    });
}

app.use(multer({ dest: './uploads/',
		 rename: function (fieldname, filename) {
		     return filename+Date.now();
		 },
		 onFileUploadStart: function (file, res) {
		     console.log('upload of ' + file.originalname + ' is starting ...')
		 },
		 onFileUploadComplete: function (file, res) {
		     console.log(file.fieldname + ' uploaded to  ' + file.path)
		     done=true;

		     fs.readFile(file.path, 'utf8', function (err,data) {
			 if (err) {
			     return console.log(err);
			 }
			 
			 var lines = data.toString().split("\n");
			 console.log("First line: " + lines[0] + " total number of lines: " + lines.length);
			 
			 if (lines[0] == "time,buyer,seller,price,size,currency,symbol,sector,bid,ask") {
			     //trades file
			     runschema("uploadtradeschema.sql", function callback() {
				 for (i = 1; i < lines.length - 1; i ++) {
				     var vals = lines[i].toString().split(",");
				     //INSERT INTO Trade VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
				     
				     var time = vals[0].substring(0, 19);
				     var stmt = db.prepare("INSERT INTO UploadTrade VALUES (NULL,?,?,?,?,?,?,?,?,?,?)");  
    				     stmt.run(time, vals[1], vals[2], vals[3], vals[4], vals[5], vals[6], vals[7], vals[8], vals[9], function(err){
					 if(err){
					     console.log(err);
					 }
				     });  
				     stmt.finalize();
				 }
			     });
			 }
			 else if (lines[0] == "time,sender,recipients") {
			     //comms file
			     runschema("uploadcommschema.sql", function callback() {
				 for (i = 1; i < lines.length - 1; i ++) {
				     var vals = lines[i].toString().split(",");
				     var time = vals[0].substring(0, 19);
				     var recievers = vals[2].split(";");
				     
				     for (r in recievers) {
					 var stmt = db.prepare("INSERT INTO UploadCommunication VALUES (NULL,?,?,?)");  
    			 		 stmt.run(time, vals[1],  recievers[r], function(err){
					     if(err){
						 console.log(err);
			 		     }     
					 });
			 		 stmt.finalize(); 
				     }  
			 	 }			 			   
			     });
			 } 
		     });
		 }
	       }));


//returns a string of exactly length 2
function pad(d) {
    return (d < 10) ? '0' + d.toString() : d.toString();
}


app.post("/upload", function (req, res) 
{
    res.send("Uploading file...");
});


app.use('/AndreeaApp', express.static("../../app"));

var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;
    
    console.log('Example app listening at http://%s:%s', host, port);
})

