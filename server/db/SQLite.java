/**
 * Created by Isheanesu Gambe on 26/02/2015.
 */

import java.sql.*;

public class SQLite {

    private Connection conn;

    public SQLite(){
        conn = createConnection();
    }

    public Connection createConnection(){
        Connection conn = null;
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:trades.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        System.out.println("Opened database successfully");

        return conn;
    }

    public Connection getConnection(){ return conn; }
}
