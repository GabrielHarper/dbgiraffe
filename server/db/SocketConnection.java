import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by Isheanesu Gambe on 01/03/2015.
 */
public class SocketConnection {

    private String host;
    private int port;

    public SocketConnection(int port){
        this.host = "cs261.dcs.warwick.ac.uk";
        this.port = port;
    }
    public BufferedReader connect()
            throws IOException {
        Socket socket = new Socket();
        String host = "cs261.dcs.warwick.ac.uk";
        BufferedReader dataStream = null;

        try{
            socket.connect(new InetSocketAddress(host, port));
            dataStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        }
        catch(UnknownHostException e) {
            System.err.println("Unknown host: " + host);
            System.exit(1);
        }
        return dataStream;
    }
}
