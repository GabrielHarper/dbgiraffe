import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.SQLException;

/**
 * Created by Isheanesu Gambe on 25/02/2015.
 */
public class CommReader extends Thread{

    public void run(){
        try{
            SocketConnection conn = new SocketConnection(1720);
            BufferedReader dataStream = conn.connect();
            QueryParser qp = new QueryParser();
            String response;
            boolean parse = false;

            while((response = dataStream.readLine()) != null){
                if(parse) {
                    String[] commInfo = response.split(",");
                    qp.parseCommQuery(commInfo);
                }else{
                    parse = true;
                }
            }
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }
}