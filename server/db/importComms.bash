echo "Formatting csv file $1"

more "$1" | sed 's/\.[0-9]\{6\}//g' > tmpfile
cat tmpfile > $1
rm tmpfile

echo "Inserting into database..."

echo " 
.mode csv
.import $1 UploadCommunication
" > tempsql.sql

sqlite3 trades.db < uploadcommschema.sql
sqlite3 trades.db < tempsql.sql
rm tempsql.sql

echo "Done"
